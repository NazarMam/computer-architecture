@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
  <title>Cabinet Status</title>
  <style>
    .cabinet-container {
      display: flex;
      flex-wrap: wrap;
      gap: 10px;
      margin-left: 78px; /* Add left margin */
      margin-right: 78px; /* Add right margin */
    }

    .cabinet-box {
      width: 50px;
      height: 50px;
      background-color: lightgray;
      border: 1px solid gray;
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: pointer;
    }

    .cabinet-box.occupied {
      background-color: red; /* Set color for occupied cabinets */
    }
  </style>
</head>
<body>

<div>
  <div class="container col-xxl-8 px-4 py-5" style="margin-top: -50px; margin-bottom: -25px">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
      <div class="col-10 col-sm-8 col-lg-6">
        <img src="pexels-enrique-silva-5203594.jpg" class="d-block mx-lg-auto img-fluid" width="900" height="600" loading="lazy">
      </div>
      <div class="col-lg-6">
        <h1 class="display-5 fw-bold lh-1 mb-3">Availability</h1>
        <p class="lead">Here you can check the status of cabinets in our hospital with ease. See which cabinets are occupied and which ones are vacant at a glance.</p>
      </div>
    </div>
  </div>

  <div class="cabinet-container">
    @for ($cabinetNumber = 1; $cabinetNumber <= 198; $cabinetNumber++)
      <div class="cabinet-box @if (isset($cabinetData[$cabinetNumber]) && $cabinetData[$cabinetNumber]) occupied @endif" data-cabinet="{{ $cabinetNumber }}">
        <span class="cabinet-number">{{ $cabinetNumber }}</span>
      </div>
    @endfor
  </div>

  <script>
    document.addEventListener('DOMContentLoaded', function () {
      const cabinetBoxes = document.querySelectorAll('.cabinet-box');

      cabinetBoxes.forEach(function (box) {
        box.addEventListener('click', function () {
          const cabinetNumber = box.dataset.cabinet;

          // Make an AJAX request to fetch the assigned patient information
          fetch(`/cabinet/${cabinetNumber}`)
            .then(response => response.json())
            .then(data => {
                // Display the assigned patient information or "No one" if not available
                const patientName = data.patientName ? data.patientName : 'no one.';
                alert(`Cabinet ${cabinetNumber} is assigned to ${patientName}`);
            })
            .catch(error => {
                console.error(error);
            });

        });
      });
    });
  </script>
</body>
</html>

<div class="container">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
      </a>
      <span class="mb-3 mb-md-0 text-muted">© 2022 Hospital Company, Inc</span>
    </div>
  </footer>
</div>
@endsection
