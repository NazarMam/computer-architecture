@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Update Patient</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('patients.update', $patient->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $patient->name }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="surname" class="form-label">Surname</label>
                            <input type="text" class="form-control" id="surname" name="surname" value="{{ $patient->surname }}" required>
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label>
                            <select class="form-select" id="status" name="status" required>
                                <option value="Healthy" {{ $patient->status == 'Healthy' ? 'selected' : '' }}>Healthy</option>
                                <option value="Sick" {{ $patient->status == 'Sick' ? 'selected' : '' }}>Sick</option>
                                <option value="Injured" {{ $patient->status == 'Injured' ? 'selected' : '' }}>Injured</option>
                                <option value="Other" {{ $patient->status == 'Other' ? 'selected' : '' }}>Other</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="assignedDoc" class="form-label">Assigned Doctor</label>
                            <select class="form-select" id="assignedDoc" name="assignedDoc" required>
                                <option value="">Select a doctor</option>
                                @foreach($doctors as $doctor)
                                    <option value="{{ $doctor->id }}" {{ $patient->assignedDoc == $doctor->id ? 'selected' : '' }}>{{ $doctor->name }} {{ $doctor->surname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="assignedCab" class="form-label">Assigned Cabinet</label>
                            <input type="number" class="form-control" id="assignedCab" name="assignedCab" value="{{ $patient->assignedCab }}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="{{ route('patient.index') }}" class="btn btn-secondary">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
