@extends('layouts.app')

@section('content')

@if (Auth::guest())
<div class="container p-5"> <div class="alert alert-primary" style="margin-top: -50px;" role="alert"> To access all features, please log-in. </div>
@endif

@if (!Auth::guest())
<div class="container p-5"> <div class="alert alert-success" style="margin-top: -50px;" role="alert"> Welcome, {{ Auth::user()->name }}! </div>

<div>
  <div class="container col-xxl-8 px-4 py-5" style="margin-top: -50px; margin-bottom: -25px">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
      <div class="col-10 col-sm-8 col-lg-6">
        <img src="pexels-tima-miroshnichenko-5452189.jpg" class="d-block mx-lg-auto img-fluid" width="900" height="600" loading="lazy">
      </div>
      <div class="col-lg-6">
        <h1 class="display-5 fw-bold lh-1 mb-3">Doctors</h1>
        <p class="lead">Here, you'll find information about the dedicated physicians who are an integral part of our hospital team.</p>
      </div>
      <button type="button" onclick="showForm()" class="w-100 btn btn-lg btn-outline-primary">Add doctor</button>
    </div>
  </div>

<script>
    function showForm() {
    // Get the popup element
    var popup = document.getElementById("popup-form");

    // Display the popup
    popup.style.display = "block";
    }
</script>


<div id="popup-form" style="text-align: center; margin-bottom: 100px; display:none;">
  <form method="post" action="{{ route('doctors.store') }}">
    @csrf
    <label for="name">Name:</label>
    <input type="text" name="name" required pattern=".{3,15}" title="Name must be between 3 and 15 characters">

    <label for="surname">Surname:</label>
    <input type="text" name="surname" required pattern=".{3,15}" title="Surname must be between 3 and 15 characters">

    <button type="submit">Submit</button>
  </form>
</div>

<div class="row justify-content-between">
<div class="col">
    <h3>Sort by</h3>
    <div class="btn-group" role="group" aria-label="Basic example">
        <a href="{{ route('doctors.index', ['sort_by' => 'name', 'sort_dir' => 'asc']) }}" class="btn btn-primary">Name ↑</a>
        <a href="{{ route('doctors.index', ['sort_by' => 'name', 'sort_dir' => 'desc']) }}" class="btn btn-primary">Name ↓</a>
        <a href="{{ route('doctors.index', ['sort_by' => 'surname', 'sort_dir' => 'asc']) }}" class="btn btn-primary">Surname ↑</a>
        <a href="{{ route('doctors.index', ['sort_by' => 'surname', 'sort_dir' => 'desc']) }}" class="btn btn-primary">Surname ↓</a>
        <a href="{{ route('doctors.index', ['sort_by' => 'patients_count', 'sort_dir' => 'asc']) }}" class="btn btn-primary">No. of Patients ↑</a>
        <a href="{{ route('doctors.index', ['sort_by' => 'patients_count', 'sort_dir' => 'desc']) }}" class="btn btn-primary">No. of Patients ↓</a>
    </div>
</div>



    <div class="col">
        <h3>Filter by</h3>
        <form id="filter-form" action="{{ route('doctors.index') }}" method="GET">
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="submit" name="has_patients" value="true" class="btn btn-primary">Has Patients</button>
                <button type="submit" name="has_patients" value="false" class="btn btn-primary">No patients</button>
                <button type="submit" name="has_patients" value="" class="btn btn-danger">Clear Filter</button>
            </div>
        </form>
        @if($doctors->isEmpty())
        <p>No matches found.</p>
        @else
        <!-- Display your patient list here -->
        @endif
    </div>
</div>

<div class="row row-cols-1 row-cols-md-3 mb-3 text-center" style="margin-top: 20px;">
    @foreach ($doctors as $doctor)
    @if ((($doctor->patients_count > 0 && request()->input('has_patients') != 'false') || request()->input('has_patients') == null)
        || ($doctor->patients_count == null && request()->input('has_patients') == 'false'))
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal"> {{ $doctor->name }} {{ $doctor->surname }} </h4>
                </div>
                <div class="card-body">
                    <img src="profile_images.png" class="d-block mx-lg-auto img-fluid" style="margin-bottom: 15px" width="200" height="150" loading="lazy">
                    @if(Auth::check())
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-sm btn-primary show-patients-btn" data-bs-toggle="modal" data-bs-target="#patientsModal{{ $doctor->id }}">Show Patients</button>
                        <form action="{{ route('doctors.destroy', $doctor->id) }}" method="POST" id="deleteDoctorForm">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger" onclick="confirmDelete(event)">Delete</button>
                        </form>

                        <script>
                          function confirmDelete(event) {
                            event.preventDefault(); // Prevent the form submission

                            if (confirm('Are you sure you want to delete the doctor?')) {
                            document.getElementById('deleteDoctorForm').submit(); // Submit the form if user confirms
                            }
                          }
                        </script>

                    </div>
                    @endif
                    <p class="card-text">
                        <small class="text-muted">
                        @if ($doctor->patients_count > 0)
                                Assigned patients: {{ $doctor->patients_count }}
                            @else
                                No assigned patients
                            @endif
                        </small>
                    </p>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="patientsModal{{ $doctor->id }}" tabindex="-1" aria-labelledby="patientsModal{{ $doctor->id }}Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="patientsModal{{ $doctor->id }}Label">Assigned patients: {{ $doctor->patients_count }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @if ($doctor->patients()->count() > 0)
                            <ul>
                                @foreach ($doctor->patients as $patient)
                                    <li>{{ $patient->name }} {{ $patient->surname }}</li>
                                @endforeach
                            </ul>
                        @else
                            No assigned patients
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @endforeach
</div>





<div class="d-flex justify-content-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @if ($doctors->currentPage() > 1)
            <li class="page-item">
                <a class="page-link" href="{{ $doctors->previousPageUrl() }}" tabindex="-1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            @endif

            @for ($i = 1; $i <= $doctors->lastPage(); $i++)
            <li class="page-item{{ ($doctors->currentPage() === $i) ? ' active' : '' }}">
                <a class="page-link" href="{{ $doctors->url($i) }}">{{ $i }}</a>
            </li>
            @endfor

            @if ($doctors->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $doctors->nextPageUrl() }}" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
            @endif
        </ul>
    </nav>
</div>

<div class="container">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
      </a>
      <span class="mb-3 mb-md-0 text-muted">© 2022 Hospital Company, Inc</span>
    </div>
  </footer>
</div>
@endif

@endsection

