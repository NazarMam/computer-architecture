@extends('layouts.app')

@section('content')

<div>
  <div class="container col-xxl-8 px-4 py-5" style="margin-top: -50px; margin-bottom: -25px">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
      <div class="col-10 col-sm-8 col-lg-6">
        <img src="pexels-rodnae-productions-6129237.jpg" class="d-block mx-lg-auto img-fluid" width="900" height="600" loading="lazy">
      </div>
      <div class="col-lg-6">
        <h1 class="display-5 fw-bold lh-1 mb-3">Hospital Patient Management Tool</h1>
        <p class="lead">Comprehensive and efficient system for managing patient information and interactions within a hospital setting.</p>
      </div>
    </div>
  </div>

 <div class="container">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
      </a>
      <span class="mb-3 mb-md-0 text-muted">© 2022 Hospital Company, Inc</span>
    </div>
  </footer>
</div>

@endsection

