@extends('layouts.app')

@section('content')

@if (Auth::guest())
<div class="container p-5"> <div class="alert alert-primary" style="margin-top: -50px;" role="alert"> To access all features, please log-in. </div>
@endif

@if (!Auth::guest())
<div class="container p-5"> <div class="alert alert-success" style="margin-top: -50px;" role="alert"> Welcome, {{ Auth::user()->name }}! </div>

<div>
  <div class="container col-xxl-8 px-4 py-5" style="margin-top: -50px; margin-bottom: -25px">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
      <div class="col-10 col-sm-8 col-lg-6">
        <img src="pexels-gustavo-fring-3985214.jpg" class="d-block mx-lg-auto img-fluid" width="900" height="600" loading="lazy">
      </div>
      <div class="col-lg-6">
        <h1 class="display-5 fw-bold lh-1 mb-3">Patients</h1>
        <p class="lead">Here, you'll find information about all the patients we're proud to care for. Quickly and easily locate patient profiles, medical records, and assigned doctors.</p>
      </div>
      <button type="button" onclick="showForm()" class="w-100 btn btn-lg btn-outline-primary">Add patient</button>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  function showForm() {
    var popup = document.getElementById("popup-form");
    popup.style.display = "block";
  }

  $(document).ready(function() {
    var isCabinetValid = false; // Flag to track cabinet validation

    $('#assignedCab').on('input', function() {
      var cabinetNumber = $(this).val();

      $.ajax({
        method: 'GET',
        url: '/check-cabinet-number',
        data: { cabinetNumber: cabinetNumber },
        async: false, // Perform the validation synchronously
        success: function(response) {
          if (response.exists) {
            $('#assignedCabError').text('The cabinet number is already taken.');
            isCabinetValid = false; // Set the flag to false if cabinet is taken
          } else {
            $('#assignedCabError').text('');
            isCabinetValid = true; // Set the flag to true if cabinet is available
          }
        },
        error: function() {
          console.log('Error checking cabinet number.');
          isCabinetValid = false; // Set the flag to false in case of an error
        }
      });
    });

    $('#patientForm').submit(function(event) {
      var name = $('#name').val();
      var surname = $('#surname').val();
      var assignedDoc = $('#assignedDoc').val();
      var assignedCab = $('#assignedCab').val();
      var isValid = true;

      if (name.length < 3 || name.length > 15) {
        $('#nameError').text('Name must be between 3 and 15 characters.');
        isValid = false;
      } else {
        $('#nameError').text('');
      }

      if (surname.length < 3 || surname.length > 15) {
        $('#surnameError').text('Surname must be between 3 and 15 characters.');
        isValid = false;
      } else {
        $('#surnameError').text('');
      }

      if (assignedDoc === null) {
        $('#assignedDocError').text('Select a doctor.');
        isValid = false;
      } else {
        $('#assignedDocError').text('');
      }

      if (assignedCab === "") {
        $('#assignedCabError').text('Enter a cabinet number.');
        isValid = false;
      } else if (assignedCab < 1 || assignedCab > 198) {
        $('#assignedCabError').text('Cabinet number must be between 1 and 198.');
        isValid = false;
      } else if (!isCabinetValid) {
        $('#assignedCabError').text('The cabinet number is already taken.');
        isValid = false;
      } else {
        $('#assignedCabError').text('');
      }

      if (!isValid) {
        event.preventDefault(); // Prevent form submission if there are validation errors
      }
    });
  });
</script>


<div id="popup-form" style="text-align: center; margin-bottom: 100px; display:none;">
    <form method="post" action="{{ route('patients.store') }}" novalidate id="patientForm">
        @csrf
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" required pattern=".{3,15}" title="Name must be between 3 and 15 characters">
        <span id="nameError" style="color: red;"></span>

        <label for="surname">Surname:</label>
        <input type="text" name="surname" id="surname" required pattern=".{3,15}" title="Surname must be between 3 and 15 characters">
        <span id="surnameError" style="color: red;"></span>

        <label for="status">Status:</label>
        <select name="status" required>
            <option value="Healthy">Healthy</option>
            <option value="Sick">Sick</option>
            <option value="Injured">Injured</option>
            <option value="Other">Other</option>
        </select>

        <label for="assignedDoc">Assigned Doctor:</label>
        <select name="assignedDoc" id="assignedDoc" required>
            <option value="" disabled selected>Select Doctor</option>
            @foreach($doctors as $doctor)
            <option value="{{ $doctor->id }}">{{ $doctor->name }} {{ $doctor->surname }}</option>
            @endforeach
        </select>
        <span id="assignedDocError" style="color: red;"></span>

        <label for="assignedCab">Assigned Cabinet Number:</label>
        <input type="number" name="assignedCab" id="assignedCab" required min="1" max="198" title="Assigned cabinet must be between 1 and 198">
        <span id="assignedCabError" style="color: red;"></span>

        <button type="submit">Submit</button>
    </form>
</div>







<div class="row justify-content-between">
  <div class="col">
    <h3>Sort by</h3>
    <div class="btn-group" role="group" aria-label="Basic example">
      <a href="{{ route('patients.index', ['sort_by' => 'name', 'sort_dir' => 'asc']) }}" class="btn btn-primary">Name ↑</a>
      <a href="{{ route('patients.index', ['sort_by' => 'name', 'sort_dir' => 'desc']) }}" class="btn btn-primary">Name ↓</a>
      <a href="{{ route('patients.index', ['sort_by' => 'surname', 'sort_dir' => 'asc']) }}" class="btn btn-primary">Surname ↑</a>
      <a href="{{ route('patients.index', ['sort_by' => 'surname', 'sort_dir' => 'desc']) }}" class="btn btn-primary">Surname ↓</a>
    </div>
  </div>

  <div class="col">
  <h3>Filter by</h3>
  <form id="filter-form" action="{{ route('patients.index') }}" method="GET">
    <div class="btn-group" role="group" aria-label="Basic example">
      <button type="submit" name="status" value="Healthy" class="btn btn-primary">Healthy</button>
      <button type="submit" name="status" value="Sick" class="btn btn-primary">Sick</button>
      <button type="submit" name="status" value="Injured" class="btn btn-primary">Injured</button>
      <button type="submit" name="status" value="Other" class="btn btn-primary">Other</button>
      <button type="submit" name="status" value="" class="btn btn-danger">Clear Filter</button>
    </div>
  </form>

  @if($patients->isEmpty())
    <p>No matches found.</p>
  @else
    <!-- Display your patient list here -->
  @endif
</div>
</div>

<div class="row row-cols-1 row-cols-md-3 mb-3 text-center" style="margin-top: 20px;">
    @foreach ($patients as $patient)
      @if ((($patient->status == request()->input('status')) || request()->input('status') == null))
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">{{ $patient->name }} {{ $patient->surname }}</h4>
                </div>
                <div class="card-body d-flex flex-column justify-content-between">
                  <img src="profile_images.png" class="d-block mx-lg-auto img-fluid" style="margin-bottom: 15px" width="200" height="150" loading="lazy">
                  @if(Auth::check())
                    <div class="d-flex justify-content-center">
                      <div class="btn-group">
                        <a href="{{ route('patients.edit', $patient->id) }}" class="btn btn-sm btn-primary">Update</a>
                        <form action="{{ route('patients.destroy', $patient->id) }}" method="POST" id="deletePatientForm">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-sm btn-danger" onclick="confirmDelete(event)">Delete</button>
                        </form>

                        <script>
                          function confirmDelete(event) {
                            event.preventDefault(); // Prevent the form submission

                            if (confirm('Are you sure you want to delete the patient?')) {
                            document.getElementById('deletePatientForm').submit(); // Submit the form if user confirms
                            }
                          }
                        </script>

                        <a href="{{ route('patients.logs', $patient->id) }}" class="btn btn-sm btn-success">Download Logs</a>
                        <form action="{{ route('patients.logsdelete', $patient->id) }}" method="POST" id="deleteLogsForm">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-sm btn-warning" onclick="confirmLogDelete(event)">Delete Logs</button>
                        </form>

                        <script>
                          function confirmLogDelete(event) {
                            event.preventDefault(); // Prevent the form submission

                            if (confirm('Are you sure you want to delete the logs?')) {
                            document.getElementById('deleteLogsForm').submit(); // Submit the form if user confirms
                            }
                          }
                        </script>

                      </div>
                    </div>
                @endif
                  <div>Status: {{ $patient->status }}</div>
                  <div>Assigned Cabinet: {{ $patient->assignedCab ? $patient->assignedCab : 'N/A' }}</div>
                  <div>Assigned Doctor: {{ $patient->doctor ? $patient->doctor->name.' '.$patient->doctor->surname : 'N/A' }}</div>
              </div>
            </div>
        </div>
      @endif
    @endforeach
</div>

<div class="d-flex justify-content-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @if ($patients->currentPage() > 1)
            <li class="page-item">
                <a class="page-link" href="{{ $patients->previousPageUrl() }}" tabindex="-1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            @endif

            @for ($i = 1; $i <= $patients->lastPage(); $i++)
            <li class="page-item{{ ($patients->currentPage() === $i) ? ' active' : '' }}">
                <a class="page-link" href="{{ $patients->url($i) }}">{{ $i }}</a>
            </li>
            @endfor

            @if ($patients->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $patients->nextPageUrl() }}" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
            @endif
        </ul>
    </nav>
</div>

<div class="container">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
      </a>
      <span class="mb-3 mb-md-0 text-muted">© 2022 Hospital Company, Inc</span>
    </div>
  </footer>
</div>
@endif

@endsection

