@extends('layouts.app')

@section('content')

@if (Auth::guest())
<div class="container p-5"> <div class="alert alert-primary" style="margin-top: -50px;" role="alert"> To access all features, please log-in. </div>
@endif

@if (!Auth::guest())
<div class="container p-5"> <div class="alert alert-success" style="margin-top: -50px;" role="alert"> Welcome, {{ Auth::user()->name }}! </div>
@endif

<div>
  <div class="container col-xxl-8 px-4 py-5" style="margin-top: -50px;">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
      <div class="col-10 col-sm-8 col-lg-6">
        <img src="pexels-freestocksorg-163944.jpg" class="d-block mx-lg-auto img-fluid" width="900" height="600" loading="lazy">
      </div>
      <div class="col-lg-6">
        <h1 class="display-5 fw-bold lh-1 mb-3">Medical Supplies</h1>
        <p class="lead">Here you can see the current stock levels of medicines and equipment available in our hospital shop. Stay informed and make informed decisions about your medical needs.</p>
      </div>
      @if (!Auth::guest())
      <button type="button" onclick="showForm()" class="w-100 btn btn-lg btn-outline-primary">Add medical supply</button>
      @endif
    </div>
  </div>

<script>
    function showForm() {
    // Get the popup element
    var popup = document.getElementById("popup-form");

    // Display the popup
    popup.style.display = "block";
    }
</script>


<div id="popup-form" style="text-align: center; margin-bottom: 100px; display:none;">
  <form method="post" action="{{ route('medicalsupplies.store') }}">
    @csrf
    <label for="prod_name">Title:</label>
    <input type="text" name="prod_name" required pattern=".{3,15}" title="Title must be between 3 and 15 characters">

    <label for="quantity">Quantity:</label>
    <input type="number" name="quantity" required min="0" max="999" title="Quantity must be between 0 and 999 numbers">

    <button type="submit">Submit</button>
  </form>
</div>

<div class="container">
    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
    @foreach ($medicalSupplies as $medicalSupply)
    <div class="col">
      <div class="card mb-4 rounded-3 shadow-sm">
        <div class="card-header py-3">
          <h4 class="my-0 fw-normal">{{ $medicalSupply->prod_name }}</h4>
        </div>
        <div class="card-body">
          <img src="unnamed.png" class="d-block mx-lg-auto img-fluid" style="margin-bottom: 15px" width="200" height="150" loading="lazy">
          <div>Left in stock: {{ $medicalSupply->quantity }}</div>
          @if (Auth::check())
          <div class="btn-group">
              <button type="button" class="btn btn-sm btn-primary update-button">Update</button>
              <form action="{{ route('medicalsupplies.destroy', $medicalSupply->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
              </form>
            </div>

            <div class="update-form" style="display: none;">
              <form action="{{ route('medicalsupplies.update', $medicalSupply->id) }}" method="POST">
                @csrf
                @method('PUT')
                <input type="text" name="quantity" value="{{ $medicalSupply->quantity }}">
                <button type="submit" class="btn btn-sm btn-primary">Save</button>
              </form>
            </div>
          @endif
        </div>
      </div>
    </div>
  @endforeach
</div>

<script>
  document.addEventListener('DOMContentLoaded', function () {
    const updateButtons = document.querySelectorAll('.update-button');

    updateButtons.forEach(function (button) {
      button.addEventListener('click', function () {
        const updateForm = button.parentNode.nextElementSibling;
        updateForm.style.display = 'block';
      });
    });
  });
</script>



<div class="d-flex justify-content-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @if ($medicalSupplies->currentPage() > 1)
            <li class="page-item">
                <a class="page-link" href="{{ $medicalSupplies->previousPageUrl() }}" tabindex="-1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            @endif

            @for ($i = 1; $i <= $medicalSupplies->lastPage(); $i++)
            <li class="page-item{{ ($medicalSupplies->currentPage() === $i) ? ' active' : '' }}">
                <a class="page-link" href="{{ $medicalSupplies->url($i) }}">{{ $i }}</a>
            </li>
            @endfor

            @if ($medicalSupplies->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $medicalSupplies->nextPageUrl() }}" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
            @endif
        </ul>
    </nav>
</div>

<div class="container">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
      </a>
      <span class="mb-3 mb-md-0 text-muted">© 2022 Hospital Company, Inc</span>
    </div>
  </footer>
</div>

@endsection

