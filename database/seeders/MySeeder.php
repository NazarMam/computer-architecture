<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MySeeder extends Seeder
{
    public function run()
    {
        DB::table('medicalsupplies')->insert([
            ['prod_name' => 'Citramon', 'quantity' => 13],
            ['prod_name' => 'Penoxicilin', 'quantity' => 1],
            ['prod_name' => 'Aspirin', 'quantity' => 0]
        ]);

        DB::table('patients')->insert([
            [
                'name' => 'John',
                'surname' => 'Doe',
                'status' => 'Healthy',
                'assignedDoc' => null,
                'assignedCab' => null,
            ],
            [
                'name' => 'Jane',
                'surname' => 'Doe',
                'status' => 'Sick',
                'assignedDoc' => null,
                'assignedCab' => null,
            ],
        ]);

        DB::table('doctors')->insert([
            [
                'name' => 'Joseph',
                'surname' => 'Cunning',
                'assignedPat' => null,
                'assignedCab' => null,
            ],
            [
                'name' => 'Jack',
                'surname' => 'King',
                'assignedPat' => null,
                'assignedCab' => null,
            ],
        ]);

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
