<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmptyPatientsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('patients')->delete();
    }
}
?>
