<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
{
    Schema::dropIfExists('patients');

    Schema::create('patients', function (Blueprint $table) {
        $table->id();
        $table->string('name', 12);
        $table->string('surname', 12);
        $table->enum('status', ['Healthy', 'Sick', 'Injured', 'Other'])->default('Healthy');
        $table->unsignedBigInteger('assignedDoc')->nullable();
        $table->unsignedBigInteger('assignedCab')->nullable();

        $table->unique('assignedCab');

        $table->foreign('assignedDoc')->references('id')->on('doctors')->onDelete('set null');
    });
}



    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
