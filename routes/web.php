<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MedicalSupplyController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\CabinetController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/homepage', [App\Http\Controllers\Homepage::class, 'index']);

Route::get('/medicalsupply', [App\Http\Controllers\MedicalSupplyController::class, 'index']);

Route::post('/medicalsupplies', [MedicalSupplyController::class, 'store'])->name('medicalsupplies.store');
Route::put('/medicalsupplies/{medicalsupply}', [MedicalSupplyController::class, 'update'])->name('medicalsupplies.update');
Route::delete('/medicalsupplies/{medicalsupply}', [\App\Http\Controllers\MedicalSupplyController::class, 'destroy'])->name('medicalsupplies.destroy');
Route::get('/medicalsupply', [MedicalSupplyController::class, 'index'])->name('medicalsupply.index');

Route::post('/patients', [PatientController::class, 'store'])->name('patients.store');
Route::delete('/patients/{patient}', [\App\Http\Controllers\PatientController::class, 'destroy'])->name('patients.destroy');
Route::get('/patient', [PatientController::class, 'index'])->name('patient.index');
Route::get('/patients', [PatientController::class, 'index'])->name('patients.index');
Route::get('/patients/{patient}/edit', [\App\Http\Controllers\PatientController::class, 'edit'])->name('patients.edit');
Route::put('/patients/{patient}', [\App\Http\Controllers\PatientController::class, 'update'])->name('patients.update');
Route::get('/patients/{patient}/logs', 'App\Http\Controllers\PatientController@logs')->name('patients.logs');
Route::delete('patients/{patient}/logs', [PatientController::class, 'deleteLogs'])->name('patients.logsdelete');
Route::get('/check-cabinet-number', 'App\Http\Controllers\PatientController@checkCabinetNumber')->name('patients.checkCabinetNumber');
Route::get('/check-unique', 'App\Http\Controllers\PatientController@checkUnique')->name('patients.checkUnique');



Route::post('/doctors', [\App\Http\Controllers\DoctorController::class, 'store'])->name('doctors.store');
Route::delete('/doctors/{doctor}', [\App\Http\Controllers\DoctorController::class, 'destroy'])->name('doctors.destroy');
Route::get('/doctor', [\App\Http\Controllers\DoctorController::class, 'index'])->name('doctor.index');
Route::get('/doctors', [\App\Http\Controllers\DoctorController::class, 'index'])->name('doctors.index');


Route::get('/cabinet', [CabinetController::class, 'index'])->name('cabinet.index');
Route::get('/cabinet/{cabinetNumber}', [\App\Http\Controllers\CabinetController::class, 'getAssignedPatient'])->name('cabinet.getAssignedPatient');







