<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    public function doctor()
    {
        return $this->belongsTo(Doctors::class, 'assignedDoc');
    }
    public function statusLogs()
    {
        return $this->hasMany(StatusLog::class);
    }
    protected $fillable = ['name', 'surname', 'status', 'assignedDoc', 'assignedCab'];
    public $timestamps = false;
    use HasFactory;
}
