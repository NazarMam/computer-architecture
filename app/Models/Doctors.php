<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    public function patients()
    {
        return $this->hasMany(Patients::class, 'assignedDoc');
    }


    public $timestamps = false;
    protected $casts = [
        'assignedPat' => 'array',
    ];    
    use HasFactory;
}
