<?php

namespace App\Http\Controllers;

use App\Models\Doctors;
use App\Models\Patients;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index(Request $request)
{
    $sortBy = $request->input('sort_by');
    $sortDir = $request->input('sort_dir');
    $hasPatients = $request->input('has_patients');

    $query = Doctors::withCount('patients');

    if ($hasPatients == 'true') {
        $query->has('patients');
    } elseif ($hasPatients == 'false') {
        $query->has('patients', '=', 0);
    }

    if ($sortBy == 'name') {
        $query->orderBy('name', $sortDir);
    } elseif ($sortBy == 'surname') {
        $query->orderBy('surname', $sortDir);
    } elseif ($sortBy == 'patients_count') {
        $query->orderBy('patients_count', $sortDir);
    } else {
        $query->orderBy('patients_count', 'desc');
    }

    $doctors = $query->paginate(12)->appends([
        'sort_by' => $sortBy,
        'sort_dir' => $sortDir,
        'has_patients' => $hasPatients,
    ]);

    return view('doctor', ['doctors' => $doctors]);
}
    public function store(Request $request)
    {
        // Validate the form data
        $validatedData = $request->validate([
            'name' => 'required|string|min:3|max:15',
            'surname' => 'required|string|min:3|max:15'
        ]);

        // Create a new medical supply
        $doctor = new Doctors();
        $doctor->name = $validatedData['name'];
        $doctor->surname = $validatedData['surname'];
        $doctor->save();

        // Redirect back to the view with a success message
        return redirect()->back()->with('success', 'Doctor added successfully!');
    }

    public function destroy(Doctors $doctor)
    {
        $doctor->delete();

        return redirect()->route('doctor.index')
            ->with('success', 'Doctor deleted successfully');
    }

    public function assignPatient(Request $request, $id)
    {
        $doctor = Doctors::findOrFail($id);
        $assignedPatients = $request->input('assignedPatients');

        $doctor->patients()->sync($assignedPatients);

        return redirect()->back()->with('success', 'Patients assigned successfully!');
    }
}



