<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Models\Patients;
use App\Models\Doctors;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Log;
use App\Models\StatusLog;

class PatientController extends Controller
{
    public function index(Request $request)
{
    $sortBy = $request->input('sort_by');
    $sortDir = $request->input('sort_dir');

    $validSortColumns = ['name', 'surname'];
    $validSortDirs = ['asc', 'desc'];

    // Validate sort_by and sort_dir inputs
    if (!in_array($sortBy, $validSortColumns)) {
        $sortBy = 'name';
    }

    if (!in_array($sortDir, $validSortDirs)) {
        $sortDir = 'asc';
    }

    $status = $request->input('status');

    $query = Patients::orderBy($sortBy, $sortDir);

    if (!empty($status)) {
        $query->where('status', $status);
    }

    $patients = $query->paginate(12);

    $doctors = Doctors::all();
    $patients->appends(['sort_by' => $sortBy, 'sort_dir' => $sortDir, 'status' => $status]);

    return view('patient', compact('patients', 'doctors'))->with([
        'sort_by' => $sortBy,
        'sort_dir' => $sortDir,
        'status' => $status,
    ]);
}

public function store(Request $request)
{
    // Validate the form data
    $validatedData = $request->validate([
        'name' => 'required|string|min:3|max:15',
        'surname' => 'required|string|min:3|max:15',
        'status' => 'required|string|min:3|max:12',
        'assignedDoc' => [
            'required',
            Rule::exists('doctors', 'id')->where(function ($query) use ($request) {
                $query->where('id', $request->input('assignedDoc'));
            })
        ],
        'assignedCab' => [
            'required',
            'numeric',
            'min:1',
            'max:198',
            Rule::unique('patients', 'assignedCab'),
        ],
    ]);

    $cabinetNumber = $validatedData['assignedCab'];
    $exists = Patients::where('assignedCab', $cabinetNumber)->exists();

    if ($exists) {
        return redirect()->back()->withInput($request->except('assignedCab'))->withErrors(['assignedCab' => 'This cabinet number is already taken.']);
    }

    // Create a new patient
    $patient = new Patients();
    $patient->name = $validatedData['name'];
    $patient->surname = $validatedData['surname'];
    $patient->status = $validatedData['status'];
    $patient->assignedDoc = $validatedData['assignedDoc'];
    $patient->assignedCab = $cabinetNumber;
    $patient->save();

    // Create new patient log
    $log = new StatusLog;
    $log->user_id = auth()->user()->id;
    $log->patients_id = $patient->id;
    $log->status = $patient->status;
    $log->save();

    // Update the assignedPat field of the assigned doctor
    $doctor = Doctors::findOrFail($validatedData['assignedDoc']);
    $assignedPat = json_decode($doctor->assignedPat, true) ?? [];
    $assignedPat[] = $patient->name . ' ' . $patient->surname;
    $doctor->assignedPat = json_encode($assignedPat);
    $doctor->save();

    // Redirect back to the view with a success message
    return redirect()->back()->with('success', 'Patient added successfully!');
}



public function checkCabinetNumber(Request $request)
{
    $cabinetNumber = $request->input('cabinetNumber');
    $exists = Patients::where('assignedCab', $cabinetNumber)->exists();
    return response()->json(['exists' => $exists]);
}

public function checkUnique(Request $request)
{
    $assignedCab = $request->input('assignedCab');
    $unique = !Patients::where('assignedCab', $assignedCab)->exists();

    return response()->json(['unique' => $unique]);
}

public function edit(Patients $patient)
{
    $doctors = Doctors::all();
    return view('patientedit', compact('patient', 'doctors'));
}

public function update(Request $request, Patients $patient)
{
    $request->validate([
        'name' => 'required|string|max:12',
        'surname' => 'required|string|max:12',
        'status' => 'required|in:Healthy,Sick,Injured,Other',
        'assignedDoc' => 'nullable|exists:doctors,id',
        'assignedCab' => [
            'required',
            'numeric',
            'min:1',
            'max:198',
            Rule::unique('patients', 'assignedCab'),
        ]  
    ]);

    $patient->update($request->only(['name', 'surname', 'status', 'assignedDoc', 'assignedCab']));

    
    // Create new patient log
    $log = new StatusLog;
    $log->user_id = auth()->user()->id;
    $log->patients_id = $patient->id;
    $log->status = $patient->status;
    $log->save();   

    return redirect()->route('patients.index')->with('success', 'Patient updated successfully.');
}

public function logs(Patients $patient)
{
    $logs = StatusLog::where('patients_id', $patient->id)->get();
    $filename = $patient->name . '_' . $patient->surname . '_logs.txt';
    $content = 'Changer | Patient | Status | Assigned Doc | Assigned Cab | Timestamp' . "\r\n";
    foreach ($logs as $log) {
        $content .= $log->user->name . ' | ' . $patient->name . ' ' . $patient->surname . ' | ' . $log->status . ' | ' . ($patient->assignedDoc ? Doctors::find($patient->assignedDoc)->name . ' ' . Doctors::find($patient->assignedDoc)->surname : '-') . ' | ' . $patient->assignedCab . ' | ' . $log->timestamp . "\r\n";

    }
    return response()->streamDownload(function() use ($content) {
        echo $content;
    }, $filename);
}

public function deleteLogs(Patients $patient)
{
    $logs = StatusLog::where('patients_id', $patient->id)->delete();
    return redirect()->back()->with('success', 'Logs for ' . $patient->name . ' ' . $patient->surname . ' have been deleted.');
}


    public function destroy(Patients $patient)
    {
        $patient->delete();

        return redirect()->route('patient.index')
            ->with('success', 'Patient deleted successfully');
    }

}
