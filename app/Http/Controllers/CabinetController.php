<?php

namespace App\Http\Controllers;

use App\Models\Patients;
use App\Models\Doctors;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CabinetController extends Controller
{
    public function index()
{
    // Fetch the cabinet information from the database or any other source
    $occupiedCabinets = Patients::pluck('assignedCab')->toArray();
    $cabinetData = [];

    // Populate the $cabinetData array with cabinet numbers and their occupancy status
    for ($cabinetNumber = 1; $cabinetNumber <= 198; $cabinetNumber++) {
        $cabinetData[$cabinetNumber] = in_array($cabinetNumber, $occupiedCabinets);
    }

    // Pass the cabinet data to the view
    return view('cabinet', ['cabinetData' => $cabinetData]);
}

  public function getAssignedPatient($cabinetNumber)
  {
    $patient = Patients::where('assignedCab', $cabinetNumber)->first();
    
    if ($patient) {
      return response()->json([
        'patientName' => $patient->name . ' ' . $patient->surname,
      ]);
    } else {
      return response()->json([
        'patientName' => 'no one.',
      ]);
    }
  }
}
