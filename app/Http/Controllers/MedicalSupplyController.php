<?php

namespace App\Http\Controllers;

use App\Models\Medicalsupply;
use Illuminate\Http\Request;

class MedicalSupplyController extends Controller
{
    public function index()
    {
        $medicalSupplies = Medicalsupply::paginate(12);
        return view('medicalsupply', compact('medicalSupplies'));
    }

    public function store(Request $request)
    {
        // Validate the form data
        $validatedData = $request->validate([
            'prod_name' => 'required|string|min:3|max:15',
            'quantity' => 'required|numeric|min:0|max:999',
        ]);

        // Create a new medical supply
        $medicalsupply = new Medicalsupply();
        $medicalsupply->prod_name = $validatedData['prod_name'];
        $medicalsupply->quantity = $validatedData['quantity'];
        $medicalsupply->save();

        // Redirect back to the view with a success message
        return redirect()->back()->with('success', 'Medical Supply added successfully!');
    }

    public function update(Request $request, Medicalsupply $medicalsupply)
    {
        // Validate the form data
        $validatedData = $request->validate([
            'quantity' => 'required|numeric|min:0|max:999',
        ]);

        // Update the quantity of the medical supply
        $medicalsupply->quantity = $validatedData['quantity'];
        $medicalsupply->save();

        // Redirect back to the view with a success message
        return redirect()->back()->with('success', 'Medical Supply quantity updated successfully!');
    }

    public function destroy(Medicalsupply $medicalsupply)
    {
        $medicalsupply->delete();

        return redirect()->route('medicalsupply.index')
            ->with('success', 'Medical Supply deleted successfully');
    }
}
